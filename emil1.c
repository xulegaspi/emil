/*
 * emil1.c
 *
 *  Created on: Oct 14, 2014
 *      Author: xulegaspi
 */

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#define PI 3.14159265358979323846264


int main (int argc, char *argv[]) {

	// Variable declaration
	double m, ni, mypi, time, total_time, pi_error, error;
	int i, ii, jj, kk, iterations, threads, cores1, rep;

	// Setting up for different configurations
	for (jj = 1; jj <= 5; jj++) {
		// Choosing number of threads used
		switch(jj) {
			case 1:
				threads = 1;
				break;
			case 2:
				threads = 2;
				break;
			case 3:
				threads = 4;
				break;
			case 4:
				threads = 8;
				break;
			case 5:
				threads = 16;
				break;
			default:
				break;
		}

		for (kk = 1; kk <= 3; kk++) {
			// Choosing number of iterations
			switch(kk) {
				case 1:
					iterations = 24000000;
					rep = 10;
					break;
				case 2:
					iterations = 48000000;
					rep = 10;
					break;
				case 3:
					iterations = 96000000;
					rep = 5;
					break;
				default:
					break;
			}

			omp_set_num_threads(threads);
			cores1 = omp_get_max_threads();

			printf("Start: [Cores: %d | Iterations: %d | Repetitions: %d]\n", cores1, iterations, rep);

			// Initializing values
			time = 0.0, total_time = 0.0, pi_error = 0.0, error = 0.0;

			for (ii = 1; ii < rep; ii++) {

				time = omp_get_wtime();

				m = 1.0 / (double)iterations;

				// Paralleled code
				#pragma omp parallel private(i,ni) // Defines the parallel region
				{
					// Defines the reduction with the sum strategy for the loop
					#pragma omp for reduction(+:mypi) schedule(static)
					for (i = 0; i < iterations; i++) {
						ni = ((double)i + 0.5) * m;
						mypi += 4.0 / (1.0 + ni * ni);
					}
				}


				mypi *= m;

				time = omp_get_wtime() - time;

				total_time += time;
				pi_error = pi_error + (mypi - PI);

			}

			error = pi_error/rep;

			printf(" - Average time: %.30f\n", total_time/rep);
			printf(" -Average error: %.30f\n", error);
			printf("-------------------------------------------------------------------\n");

		}
	}
	return 0;
}

